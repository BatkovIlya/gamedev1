package ru.bia.WOW;

import java.util.Scanner;

public class Shoot {
    static Scanner sc = new Scanner(System.in);
    public static void hit(){
        int i=0;
        int j=0;
        int hit1s = 0;
        int hit2s = 0;
        int hit3s = 0;
        int hit4s = 0;
        while(i<20){
            System.out.println("Введите координату X(1-10");
            int x = sc.nextInt();
            while(x<1||x>10){
                System.out.println("Корректно введите координату X(1-10");
                x = sc.nextInt();
            }
            System.out.println("Введите координату Y (1-10)");
            int y =sc.nextInt();
            while (y<1||y>10){
                System.out.println("Корректно введите координату Y (1-10)");
                y =sc.nextInt();
            }



            PlaceShip.table[y][x].setHitting(true);
            if(PlaceShip.table[y][x].getFill()&PlaceShip.table[y][x].getHitting()&PlaceShip.table[y][x].isTake4s()){
                hit4s++;
                System.out.println("Вы попали!");
                if(hit4s==4){
                    System.out.println("Вы потопили четырехпалубный корабль!");
                }
                System.out.println("Стреляй, стреляй еще!");
                i++;
                j++;


            }else if(!PlaceShip.table[y][x].getFill()&PlaceShip.table[y][x].getHitting()) {
                System.out.println("Вы не попали :(");
                System.out.println("Попробуйте еще раз :)");
                j++;
            }else if(PlaceShip.table[y][x].getFill()&PlaceShip.table[y][x].getHitting()&PlaceShip.table[y][x].isTake3s()){
                hit3s++;
                System.out.println("Вы попали!");
                if(hit3s==3){
                    System.out.println("Вы потопили трехпалубный корабль");
                }else if(hit3s==6){
                    System.out.println("Вы потопили все трехпалубные корабли!");
                }
                System.out.println("Стреляй, стреляй еще!");
                i++;
                j++;
            }else if(PlaceShip.table[y][x].getFill()&PlaceShip.table[y][x].getHitting()&PlaceShip.table[y][x].isTake2s()) {
                hit2s++;
                System.out.println("Вы попали!");
                if(hit2s==2){
                    System.out.println("Вы потопили двухпалубный корабль");
                }else if(hit2s==4){
                    System.out.println("Вы потопили второй двухпалубный корабль");
                }else if(hit2s==6){
                    System.out.println("Вы потопили все двухпалубные корабли");
                }
                System.out.println("Стреляй, стреляй еще!");
                i++;
                j++;
            }else if(PlaceShip.table[y][x].getFill()&PlaceShip.table[y][x].getHitting()&PlaceShip.table[y][x].isTake1s()) {
                hit1s++;
                if(hit1s==1){
                    System.out.println("Вы потопили однопалубный корабль");
                }else if(hit1s==2){
                    System.out.println("Вы потопили второй однопалубный корабль");
                }else if(hit1s==3){
                    System.out.println("Вы потопили третий однопалубный корабль");
                }else if(hit1s==4){
                    System.out.println("Вы потопили четвертый однопалубный корабль");
                }
                System.out.println("Стреляй, стреляй еще!");
                i++;
                j++;
            }
            PlaceShip.output();
        }
        System.out.println("Вы разгромили " +i +" палуб кораблей за " + j + " Ходов");
    }
}
