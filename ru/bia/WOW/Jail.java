package ru.bia.WOW;

public class Jail {

    private int y;
    private int x;
    private int equal;
    private boolean fill;
    private boolean hitting;
    private boolean take1s;
    private boolean take2s;
    private boolean take3s;
    private boolean take4s;

    public Jail(int y, int x, int equal, boolean fill, boolean hitting, boolean take1s, boolean take2s, boolean take3s, boolean take4s) {
        this.y = y;
        this.x = x;
        this.equal = equal;
        this.fill = fill;
        this.hitting = hitting;
        this.take1s = take1s;
        this.take2s = take2s;
        this.take3s = take3s;
        this.take4s = take4s;
    }

    public boolean getFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public boolean getHitting() {
        return hitting;
    }

    public void setHitting(boolean hitting) {
        this.hitting = hitting;
    }

    public void setTake1s(boolean take1s) {
        this.take1s = take1s;
    }

    public void setTake2s(boolean take2s) {
        this.take2s = take2s;
    }

    public void setTake3s(boolean take3s) {
        this.take3s = take3s;
    }

    public void setTake4s(boolean take4s) {
        this.take4s = take4s;
    }

    public boolean isTake1s() {
        return take1s;
    }

    public boolean isTake2s() {
        return take2s;
    }

    public boolean isTake3s() {
        return take3s;
    }

    public boolean isTake4s() {
        return take4s;
    }

    @Override
    public String toString() {
        if (hitting) {
            return "X ";
        } else if (hitting & fill) {
            return "O ";
        } else if (equal != 0 & fill) {
            return equal+" ";
        }else if(equal==10||equal==11){
            return equal + " ";
        }else if (fill) {
            return "B ";
        } else {
            return "~ ";

        }

    }
}