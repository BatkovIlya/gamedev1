package ru.bia.WOW;

import java.util.Scanner;

public class PlaceShip {
    private static int SIZE = 12;
    static Scanner sc = new Scanner(System.in);

     static Jail [][] table = new Jail[SIZE][SIZE];

     public static void output(){
         for (int i = 0; i<SIZE;i++){
             for(int j = 0;j<SIZE;j++){
                 System.out.print(table[i][j]);
             }
             System.out.println();
         }
    }
    public static void input() {
        int q = 0;
        for (int i = 0; i < SIZE; i++) {
            q++;
            for (int j = 0; j < SIZE; j++) {
                if(i==0){
                    table[i][j] = new Jail(i, j,j, true, false, false, false, false, false);
                }else if(i==11){
                    table[i][j] = new Jail(i, j,j, true, false, false, false, false, false);
                }else if(j==0){
                    table[i][j] = new Jail(i, j,i, true, false, false, false, false, false);
                }else if(j==11){
                    table[i][j] = new Jail(i, j,i, true, false, false, false, false, false);
                }else {
                    table[i][j] = new Jail(i, j, 0, false, false, false, false, false, false);
                }

            }
        }
    }
    public static void place4x() {
        int x = 0;
        int y = 0;
        x = 1+(int)(Math.random()*10);
        y = 1+(int)(Math.random()*10);
        turn4x(x, y);
        System.out.println("Четырехпалубный поставлен");
    }

    public static void turn4x(int x, int y) {
        int direction;
        direction = 1+(int)(Math.random()*4);
        switch (direction) {
            case 1: /* право **/
                while(x == 9|| x==10|| x==11||x==8) {
                    while(!table[x][y].getFill()||!table[x][y-1].getFill()||!table[x][y+1].getFill()||!table[x-1][y].getFill()||!table[x-1][y-1].getFill()||!table[x-1][y+1].getFill()||!table[x+1][y].getFill()||!table[x+2][y].getFill()||!table[x+3][y].getFill()||!table[x+4][y].getFill()||!table[x+1][y-1].getFill()||!table[x+2][y-1].getFill()||!table[x+3][y-1].getFill()||!table[x+4][y-1].getFill()||!table[x+1][y+1].getFill()||!table[x+2][y+1].getFill()||!table[x+3][y+1].getFill()||!table[x+4][y+1].getFill()){
                        System.out.println("Сюда нельзя поставить корабль, так как вокруг него уже есть корабль, измените начальную точку");
                        place4x();
                    }
                    System.out.println("Поверните в другую сторону, корабль выходит за пределы клетки ");
                    turn4x(x,y);
                }
                right4x(x, y);

                break;
            case 2: /* лево **/
                while(x == 0|| x == 1||x==2||x==3) {
                    while(!table[x][y].getFill()||!table[x][y-1].getFill()||!table[x][y+1].getFill()||!table[x+1][y].getFill()||!table[x-1][y+1].getFill()||!table[x+1][y+1].getFill()||!table[x-1][y].getFill()||!table[x-1][y-1].getFill()||!table[x-1][y+1].getFill()||!table[x-2][y].getFill()||!table[x-2][y-1].getFill()||!table[x-2][y-1].getFill()||!table[x-3][y].getFill()||!table[x-3][y-1].getFill()||!table[x-3][y+1].getFill()||!table[x-4][y].getFill()||!table[x-4][y-1].getFill()||!table[x-4][y+1].getFill()){
                        System.out.println("Сюда нельзя поставить корабль, так как вокруг него уже есть корабль, измените начальную точку");
                        place4x();
                    }
                    System.out.println("Поверните в другую сторону, корабль выходит за пределы клетки ");
                    turn4x(x,y);
                }
                left4x(x,y);
                break;
            case 3: /* вниз **/
                while(y == 9 || y == 8 ||y==10||y==11) {
                    while(!table[x][y].getFill()||!table[x-1][y].getFill()||!table[x+1][y].getFill()||!table[x][y-1].getFill()||!table[x-1][y-1].getFill()||!table[x+1][y-1].getFill()||!table[x][y+1].getFill()||!table[x+1][y+1].getFill()||!table[x-1][y+1].getFill()||!table[x][y+2].getFill()||!table[x-1][y+2].getFill()||!table[x+1][y+2].getFill()||!table[x][y+3].getFill()||!table[x-1][y+3].getFill()||!table[x+1][y+3].getFill()||!table[x][y+4].getFill()||!table[x-1][y+4].getFill()||!table[x+1][y+4].getFill()){
                        System.out.println("Сюда нельзя поставить корабль, так как вокруг него уже есть корабль, измените начальную точку");
                        place4x();
                    }
                    System.out.println("Поверните в другую сторону, корабль выходит за пределы клетки ");
                    turn4x(x,y);
                }
                down4x(x,y);
                break;
            case 4: /* вверх **/
                while(y == 1 || y == 0 || y == 2||y==3) {
                    while(!table[x][y].getFill()||!table[x-1][y].getFill()||!table[x+1][y].getFill()||!table[x][y-1].getFill()||!table[x-1][y-1].getFill()||!table[x+1][y-1].getFill()||!table[x][y+1].getFill()||!table[x+1][y+1].getFill()||!table[x-1][y+1].getFill()|!table[x][y-2].getFill()||!table[x-1][y-2].getFill()||!table[x+1][y-2].getFill()||!table[x][y-3].getFill()||!table[x-1][y-3].getFill()||!table[x+1][y-3].getFill()||!table[x][y-4].getFill()||!table[x-1][y-4].getFill()||!table[x+1][y-4].getFill()){
                        System.out.println("Сюда нельзя поставить корабль, так как вокруг него уже есть корабль, измените начальную точку");
                        place4x();
                    }
                    System.out.println("Поверните в другую сторону, корабль выходит за пределы клетки ");
                    turn4x(x,y);
                }
                up4x(x,y);
                break;
        }

    }

    public static void down4x(int x, int y) {
        table[x][y].setFill(true);
        table[x - 1][y].setFill(true);
        table[x - 2][y].setFill(true);
        table[x - 3][y].setFill(true);
        table[x][y].setTake4s(true);
        table[x - 1][y].setTake4s(true);
        table[x - 2][y].setTake4s(true);
        table[x - 3][y].setTake4s(true);
    }

    public static void up4x(int x, int y) {
        table[x][y].setFill(true);
        table[x + 1][y].setFill(true);
        table[x + 2][y].setFill(true);
        table[x + 3][y].setFill(true);
        table[x][y].setTake4s(true);
        table[x + 1][y].setTake4s(true);
        table[x + 2][y].setTake4s(true);
        table[x + 3][y].setTake4s(true);
    }

    public static void left4x(int x, int y) {
        table[x][y].setFill(true);
        table[x][y - 1].setFill(true);
        table[x][y - 2].setFill(true);
        table[x][y - 3].setFill(true);
        table[x][y].setTake4s(true);
        table[x][y - 1].setTake4s(true);
        table[x][y - 2].setTake4s(true);
        table[x][y - 3].setTake4s(true);
    }

    public static void right4x(int x, int y) {
        table[x][y].setFill(true);
        table[x][y + 1].setFill(true);
        table[x][y + 2].setFill(true);
        table[x][y + 3].setFill(true);
        table[x][y].setTake4s(true);
        table[x][y + 1].setTake4s(true);
        table[x][y + 2].setTake4s(true);
        table[x][y + 3].setTake4s(true);
    }

}